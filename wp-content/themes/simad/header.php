<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
      <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/images/favicon.ico">
      <link rel="stylesheet" type="text/css" href="https://fonts.google.com/specimen/Lato">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/slider-pro.min.css">
      <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/remodal.css">
  	  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/remodal-default-theme.css">
      <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/font/fonts.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/animate.css">
	  <?php wp_head(); ?>
      
      
      
      <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/owl.carousel.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery.sliderPro.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/js/remodal.min.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/build/js/tabs.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/script.js"></script>
      <script src="<?php bloginfo( 'template_url' ); ?>/wow.js"></script>
      <script>
            new WOW().init();
      </script>
</head>

<body <?php body_class(); ?>>


<nav class="site-main-navigation">
		
		<div class="nav-top-info-bar">
			<div class="container">	
            	<?=((get_field('telephone',130)) ? '<span>Toll-free line <a href="tel:'.str_replace(' ', '', get_field('telephone',130).'">'.get_field('telephone',130)).'</a></span>' : '' ); ?> 		
				
				<div class="search-and-social-media">
					<ul>
                    	<?php
									 // The Query
									query_posts( array ( 'post_type' => 'social-link-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
									<?php /* Start the Loop */ ?>
									<?php while ( have_posts() ) : the_post(); ?>                               
                                    <li><a target="_blank" href="<?=((get_field('link',$post->ID)) ? get_field('link',$post->ID) : '' ); ?>"><i class="fa <?=((get_field('class_name',$post->ID)) ? get_field('class_name',$post->ID) : '' ); ?>" ></i></a></li>
									 <?php endwhile; ?>
									<?php wp_reset_query(); ?>                                    
						
					</ul>
                    
    <form role="search" action="<?php echo site_url('/'); ?>" method="get" class="nav-search-form" id="searchform">
   <div class="input-wrpr"> <input type="text" name="s" placeholder="Search"/>
    <input type="hidden" name="post_type" value="post" /> <!-- // hidden 'products' value -->
    <button type="submit"><i class="fa fa-search"></i></button></div>
  </form>

					
				</div>
			</div>
		</div>
		<div class="nav-logo-wrapper-main">
			<div class="container">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/simad-university-logo.png" alt="SIMAD LOGO">
					<span>The Fountain of <br>
					Knowledge and <br>
					Wisdom</span>
				</a>
                <?php wp_nav_menu( array( 'theme_location' => 'topmenu', 'menu_class' => 'top-list', 'menu_id' => 'top-menu' ) ); ?>		
				<?php /*?><ul>
					<li><a href="#">my su</a></li>
					<li><a href="#">su alumni</a></li>
					<li><a href="faq.html">faq</a></li>
					<li><a href="#">disclaimer</a></li>
					<li><a href="media.html">media</a></li>
				</ul><?php */?>
			</div>
		</div>

		<div class="site-nav-links-main-container">
			<div class="container">	
            	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'site-main-navigation-links-list', 'menu_id' => 'primary-menu' ) ); ?>		
				<?php /*?><ul class="site-main-navigation-links-list">
					<li class="active"><a href="index.html">HOME</a></li>
					<li><a href="about.html">ABOUT US</a></li>
					<li><a href="discover.html">DISCOVER SU</a></li>
					<li><a href="academics.html">ACADEMICS</a></li>
					<li><a href="admission.html">ADMISSION</a></li>
					<li><a href="governance.html">GOVERNANCE</a></li>
					<li><a href="research.html">RESEARCH</a></li>
					<li><a href="corporates.html">CORPORATE</a></li>
					<li><a href="graduates.html">GRADUATES</a></li>
					<li><a href="contact.html">CONTACT US</a></li>
				</ul><?php */?>
				<a href="#" class="hamburger">
					<span></span>
					<span></span>
					<span></span>
				</a>
			</div>
		</div>

	</nav>
    <?php if(!is_front_page()){ 
	$category = get_queried_object();	?>
    <div class="breadcrumb-main-wrpr">
        <div class="container">
            <ul>
                <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Simad University</a></li>
                <?php if ( is_single() ){ ?>
                <?=(($category->name) ? '<li><a href="'.get_category_link($category->term_id).'">'.$category->name.'</a></li>' : ''); ?>
                <li><?=$post->post_title; ?></li>
                <?php }elseif(is_search()){ ?>
                 <li>Search</li>
                <?php }else{ ?>
                <li><?=(($category->name) ? $category->name : $post->post_title); ?></li>
               	<?php } ?>
            </ul>
        </div>
    </div>
    <?php } ?>