<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="social-media-updates-container">
	
	<div class="container">
		
		<div class="row">
			
			<div class="col-md-4">
				<header class="facebook-insta-social-feeds-headers facebook-header">
					<h2>Facebook</h2>
				</header>
				<div class="fb-page" data-href="http://www.facebook.com/simaduni1999" data-tabs="timeline" data-height="305" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="http://www.facebook.com/simaduni1999" class="fb-xfbml-parse-ignore"><a href="http://www.facebook.com/simaduni1999">SIMAD University</a></blockquote></div>
			</div>

			<div class="col-md-8">
				<header class="facebook-insta-social-feeds-headers instagram-header">
					<h2>Instagram</h2>
				</header>
				<a href="">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/instagram-feeds.jpg" alt="facebook image">
				</a>
			</div>

		</div>

	</div>

</div>
<footer class="site-footer-main">
	<div class="container">
		<div class="row footer-internal-site-links-container">
			 <?php						
						$menu_footer = array(); //menu slug
						$menu = 'Footermenu';
						$args = array(
								'order'                  => 'ASC',
								'orderby'                => 'menu_order',
								'post_type'              => 'nav_menu_item',
								'post_status'            => 'publish',
								'output'                 => ARRAY_A,
								'output_key'             => 'menu_order',
								'nopaging'               => true,
								'update_post_term_cache' => false );
						$items = wp_get_nav_menu_items( $menu, $args );						
						foreach($items as $row){
							if($row->menu_item_parent==0){
							$menu_footer[$row->ID]['details'] = $row;
							}else{
							$menu_footer[$row->menu_item_parent]['sub_menu'][$row->ID] = $row;
							}
						}						
						?>
                        <?php $ifoot =1; foreach($menu_footer as $foot_menu){
								//print_r($foot_menu);
								echo '<div class="site-internal-links-wrpr"> <h3>'.$foot_menu['details']->title.'</h3>';
								if($foot_menu['sub_menu']){
									echo '<ul>';
									foreach($foot_menu['sub_menu'] as $foot_submenu){
										echo '<li><a href="'.$foot_submenu->url.'">'.$foot_submenu->title.'</a></li>';
									}
									echo '</ul>';
								}
								echo '</div>';
								$ifoot++;
						}
						$post_contact = 130;
						?>  
			

			<div class="footer-subscribe-news-letter">
				<header>
					<h3>Subscribe Newsletter</h3>
					<p>Subscribe &amp; Get a Free SIMAD Bulletin</p>
				</header>	
                
				<div class="footer-subscribe-newsletter-form">
                	<?=do_shortcode('[contact-form-7 id="65" title="Newsletter"]'); ?>					
				</div>
			</div>

		</div>
	</div>
	<div class="footer-contact-info-bar">
		<div class="container">
			<div class="row">
				<div class="footer-contact-no-fax-wrapper">
					<h3>Contact Simad University: </h3>
					<ul>
                    	<?=((get_field('telephone',$post_contact)) ? '<li><span>T: </span><a href="">'.get_field('telephone',$post_contact).'</a></li>' : '' ); ?>  
                        <?=((get_field('fax',$post_contact)) ? '<li><span>F: </span><a href="">'.get_field('fax',$post_contact).'</a></li>' : '' ); ?>
                        <?=((get_field('email_address',$post_contact)) ? '<li><span>E: </span><a href="'.get_field('email_address',$post_contact).'">'.get_field('email_address',$post_contact).'</a></li>' : '' ); ?>
					</ul>				
				</div>
				<div class="footer-social-media-wrapper">
					<h3>Connect With Us:</h3>
					<ul>
						<?php
									 // The Query
									query_posts( array ( 'post_type' => 'social-link-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
									<?php /* Start the Loop */ ?>
									<?php while ( have_posts() ) : the_post(); ?>                               
                                    <li><a target="_blank" href="<?=((get_field('link',$post->ID)) ? get_field('link',$post->ID) : '' ); ?>"><i class="fa <?=((get_field('class_name',$post->ID)) ? get_field('class_name',$post->ID) : '' ); ?>" ></i></a></li>
									 <?php endwhile; ?>
									<?php wp_reset_query(); ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright-bar">
		<div class="container">
			<div class="row">
				<p>Copyright &copy; <?=date('Y'); ?> - SIMAD UNIVERSITY. <br> All Rights Reserved</p>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
