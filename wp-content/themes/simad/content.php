<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<?php if ( is_single() ) : ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-media-details-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
				
		</div>
	</div>
</div>


<div class="member_main">
	<div class="container">
		<div class="row">
			<div class="member_main_inner">
				<div class="col-lg-12 head_bott">
					<main class="col-md-12 bp-main-container">

						<header class="bp-main-header bp-media-date-header">
							<div class="date-wrpr">
								<span><?=date('M', strtotime($post->post_date));?></span>
								<span><?=date('d', strtotime($post->post_date));?></span>
							</div>
							<?php the_title( '<p>', '</p>' ); ?>		
						</header>
						
						<div class="media-details-main-container">

							<div class="<?=((get_field('right_side_description',$post->ID)) ? 'col-md-6' : 'col-md-12' ); ?> media-details-writeup">
								<?php echo apply_filters('the_content', $post->post_content); ?>                                								
							</div>
                            <?=((get_field('right_side_description',$post->ID)) ? '<div class="col-md-6 media-details-writeup">'.get_field('right_side_description',$post->ID).'</div>' : '' ); ?>					

						</div>

					</main>
					
				</div>
			</div>	
		</div>
	</div>
</div>


<?php else : ?>
<div id="post-<?php the_ID(); ?>" class="col-md-4 col-sm-6">
						
						<a href="<?=$post->guid; ?>" class="media-indi-post">
							<header>
								<div class="date-wrpr">
									<span><?=date('M', strtotime($post->post_date));?></span>
									<span><?=date('d', strtotime($post->post_date));?></span>
								</div>
                                <?php the_title( '<h3>', '</h3>' ); ?>								
							</header>
                            <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
							  if ($image) : ?>
							   <div class="img-wrpr"><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>                                                     
							  <?php endif; ?> 
							<p><?=content(32); ?></p>
						</a>
</div>

				
<?php endif; ?>
