<!--slider start-->

jQuery(function ($){
	jQuery('#myCarousel').carousel({
	  interval: 40000
	});

	jQuery('.carousel .item').each(function(){
	  var next = jQuery(this).next();

	  if (!next.length) {
		next = jQuery(this).siblings(':first');
	  }
	  next.children(':first-child').clone().appendTo(jQuery(this));

	  if (next.next().length>0) {

		  next.next().children(':first-child').clone().appendTo(jQuery(this)).addClass('rightest');

	  }
	  else {
		  jQuery(this).siblings(':first').children(':first-child').clone().appendTo(jQuery(this));

	  }
	  });


	//  Banner slider
	$( '#home-simad-banner' ).sliderPro({
		width: '100%',
		height: 580,
		fade:true,
		slideDistance:0,
		arrows: false,
		buttons: false,
		waitForLayers: true,
		autoplay: false,
		autoScaleLayers: false,
		breakpoints: {
			500: {

			}
		}
	}); 

	// HOME BANNER NAV CODE 
	$( '#home-simad-banner' ).on( 'gotoSlide', function( event ) {
	    console.log( event.index );
	    changeCaptionText(event.index)
	});

	$('.slider-dots > li > a').on('click' , function(){
		$this = $(this);
		$this.parent().siblings().removeClass('active');
		$this.parent().addClass('active');
		var idx = $this.parent().index();
		changeCaptionText(idx);
		
		/*$('.home-banner-caption-box').fadeIn(500).fadeOut(600);
		$('.home-banner-caption-box').fadeIn(1000);*/

		
		$( '#home-simad-banner' ).sliderPro( 'gotoSlide', idx );
		return false;
	});
	
	
	function changeCaptionText(idx) {
		if (idx || idx === 0) {
			console.log(idx);
			$('.home-banner-caption-box .indi-caption-box').removeClass('active');
			$('.home-banner-caption-box .indi-caption-box').eq(idx).addClass('active');
		}
	}
   // END HOME BANNER NAV CODE


   $('#our-testimonials').owlCarousel({
	    center: false,
	    items:3,
	    loop:true,
	    margin:35,
	    rtl: false,
	    navigation:true,
	    nav: true,
        navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
	    responsive:{
	        1170:{
	            items:2
	        },
	        490: {
	        	center: false,
	        	items:2
	        },
	        300: {
    		    center: true,
            	items:1
            }
	    }
	}); 

   	$(window).on('scroll' , function(){

   		if ($(window).scrollTop() > 100) {
   			$('.site-main-navigation').addClass('scrollnav');
   		} else {
   			$('.site-main-navigation').removeClass('scrollnav');
   		}

   	});


   	// Hamburger code 
   	$('.hamburger').on('click' , function(){
   		$('.site-main-navigation-links-list').toggleClass('open-nav');
   		return false;
   	});


   	// Discover page owl carousel
   	$('.simad-member-universities').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    autoplay: true,
	    responsive:{
	        0:{
	            items:3
	        },
	        600:{
	            items:6
	        },
	        1000:{
	            items:6
	        }
	    }
	});

});

// membership discover slider


	jQuery(function ($){
		jQuery( '#membership_discover_slider' ).sliderPro({
			width: "411px",
			height: 233,
			/*visibleSize: '100%',
			forceSize: 'fullWidth',*/
			slideDistance:0,
			autoSlideSize: true
		});

		// instantiate fancybox when a link is clicked
		jQuery( '#membership_discover_slider .sp-image' ).parent( 'a' ).on( 'click', function( event ) {
			event.preventDefault();

			// check if the clicked link is also used in swiping the slider
			// by checking if the link has the 'sp-swiping' class attached.
			// if the slider is not being swiped, open the lightbox programmatically,
			// at the correct index
			if ( jQuery( '#membership_discover_slider' ).hasClass( 'sp-swiping' ) === false ) {
				jQuery.fancybox.open( $( '#membership_discover_slider .sp-image' ).parent( 'a' ), { index: $( this ).parents( '.sp-slide' ).index() } );
			}
		});


		// FAQ page accordion
		$('.faq-indivisual-wrpr > .faq-heading').on('click' , function(){


			var $this = $(this);
			
			if ($this.parent().hasClass('active')) {
				$this.next('.faq-content-wrpr').slideUp(500 , function(){
					$this.parent().removeClass('active');
				});

				return false;
			}
			
			$this.parent().siblings().find('.faq-content-wrpr').slideUp();
			$this.parent().siblings().removeClass('active');
			$this.next('.faq-content-wrpr').slideDown(500 , function(){
				$this.parent().addClass('active');
			});

			return false;

		});
		
		$(window).load(function(){
			// Remove the # from the hash, as different browsers may or may not include it
			var hash = location.hash.replace('#','');
			if(hash != ''){
				console.log(hash);
				$('a.'+hash).click();
				//$('.youth-courses-tabs-section ul li.'+hash+' a').click();
			   }
		   });
		
	});
	
	
    /*$(document).ready(function() {
      $('.inner1').hide().delay(2000).fadeIn(2200);
	});
	
	$(document).ready(function() {
      $('.inner2').hide().delay(4000).fadeIn(2200);
	});
	

	
	$(document).ready(function() {
      $('.inner4').hide().delay(2000).fadeIn(2200);
	});
	
	
	$(document).ready(function() {
      $('.inner5').hide().delay(4000).fadeIn(2200);
	});
	

	$(document).ready(function() {
      $('.inner7').hide().delay(2000).fadeIn(2200);
	});
	
	$(document).ready(function() {
      $('.inner8').hide().delay(4000).fadeIn(2200);
	});
	
	
	$(document).ready(function() {
      $('.inner3').hide().delay(6000).fadeIn(2200);
	});
	
	$(document).ready(function() {
      $('.inner6').hide().delay(6000).fadeIn(2200);
	});*/
	
