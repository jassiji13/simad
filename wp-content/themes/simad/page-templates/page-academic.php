<?php
/**
 * Template Name: Academic Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>
<div class="member_main bp-main-container">
	<div class="container">
		<div class="row">
        	<div class="member_main_inner academincs-main-container">
				<div class="col-lg-12 head_bott bp-accordion-nav-tabs-container">
					<div class="col-lg-3 bgm_whole1">
						<div class="member1">
							<nav id="nav">
								<ul class="dicover_submenus hover_stat_color nav">
                                	<?php
												 // The Query
												 $home_sl = 1;
												query_posts( array ( 'post_type' => 'academic-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  <li <?=(($home_sl==1) ? 'class="active"' : ''); ?>><a class="academic<?=$post->ID; ?>" data-toggle="tab" href="#academic<?=$post->ID; ?>"><?php the_title();?></a></li>								  
								  <?php $home_sl++; endwhile; ?>
								  <?php wp_reset_query(); ?>
								</ul>
							</nav>
						</div>
					</div>
					
					
					

					
					<div class="tab-content">
						
                        <?php
												 // The Query
												 $home_sltab = 1;
												query_posts( array ( 'post_type' => 'academic-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  
                                  <div id="academic<?=$post->ID; ?>" class="tab-pane fade in <?=(($home_sltab==1) ? 'active' : ''); ?>">
							<div class="col-lg-9 under_div">
								<div class="member2">
									<div class="acdemic_head1">
										<h2><?php the_title();?></h2>
									</div>
									<div class="acdemic_content1">
                                    	<?=((get_field('short_description',$post->ID)) ? '<p  class="add_para_sep1">'.get_field('short_description',$post->ID).' </p>' : '' ); ?>										
										<?php echo apply_filters('the_content', $post->post_content); ?>
									</div>
                                    <?php if(get_field('lab_list',$post->ID)){ ?>
                                        <div class="acdemic_content1">
                                            <h3 class="add_para_sep2">Labs</h3>
                                            <?=((get_field('lab_heading',$post->ID)) ? '<p  class="add_para_sep3">'.get_field('lab_heading',$post->ID).' </p>' : '' ); ?>										
                                            <nav class="chang_cls_bgnd">
                                            	<?php $list_lab = explode(PHP_EOL,get_field('lab_list',$post->ID)); ?>
                                                <ul><?php foreach($list_lab  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                            </nav>
                                        </div>
                                    <?php } ?>
                                    
                                    <?php if(get_field('programs_list',$post->ID)){ ?>
                                        <div class="acdemic_content1">
                                            <h3 class="add_para_sep2">Programs</h3>
                                            <?=((get_field('programs_heading',$post->ID)) ? '<p  class="add_para_sep3">'.get_field('programs_heading',$post->ID).' </p>' : '' ); ?>										
                                            <nav class="chang_cls_bgnd">
                                            	<?php $list_f = explode(PHP_EOL,get_field('programs_list',$post->ID)); ?>
                                                <ul><?php foreach($list_f  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                            </nav>
                                        </div>
                                    <?php } ?>
								</div>
							</div>
						</div>
								  <?php $home_sltab++; endwhile; ?>
								  <?php wp_reset_query(); ?>
                        
                        
						

						
						
						
						
						
						
						
						
						
						
						
						

						

						
						
						
						
						
						
					</div>
					
					
				</div>
			</div>
        </div>
    </div>
</div>
<?php
get_footer();

