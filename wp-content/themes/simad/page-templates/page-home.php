<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="home-simad-banner-main">
  <div id="home-simad-banner" class="home-simad-banner slider-pro">
    <div class="sp-slides">
      <?php
					 // The Query
					 $home_sl = 1;
					query_posts( array ( 'post_type' => 'home-slide' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
      <?php /* Start the Loop */ ?>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                          if ($image) : ?>
      <div class="sp-slide"><img class="sp-image" src="<?php echo $image[0]; ?>" data-src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>
      <?php $home_sl++; endif; ?>
      <?php endwhile; ?>
      <?php wp_reset_query(); ?>
    </div>
  </div>
  <div class="home-banner-caption-box">
    <?php
					 // The Query
					 $home_sl_c = 0;
					query_posts( array ( 'post_type' => 'home-slide' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
    <?php /* Start the Loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>
    <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                          if ($image) : ?>
    <div class="indi-caption-box <?=(($home_sl_c==0) ? 'active' : ''); ?>" data-slider-id="<?=$home_sl_c; ?>">
      <h1>
        <?php the_title();?>
      </h1>
      <?php echo apply_filters('the_content', $post->post_content); ?>
      <?=((get_field('link',$post->ID)) ? '<a href="'.get_field('link',$post->ID).'">Read more about our Vision and Mission</a>' : '' ); ?>
    </div>
    <?php $home_sl_c++; endif; ?>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
    <ul class="slider-dots">
      <?php
					 // The Query
					 $home_sl_li = 0;
					query_posts( array ( 'post_type' => 'home-slide' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
      <?php /* Start the Loop */ ?>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                          if ($image) : ?>
      <li <?=(($home_sl_li==0) ? 'class="active"' : ''); ?>><a href=""></a></li>
      <?php $home_sl_li++; endif; ?>
      <?php endwhile; ?>
      <?php wp_reset_query(); ?>
    </ul>
  </div>
</div>
<div class="news_main wow fadeInUp">
  <div id="grid" class="container">
    <div class="row">
      <div class="col-lg-8 mobiles1 head_bott">
        <div class="col-sm-12 header_part pull-left head_bott"> <img class="img-responsive pull-left icon_img" src="<?php bloginfo( 'template_url' ); ?>/images/news.png" alt="icon">
          <h3 class="title_lvl1">News & Events</h3>
          <p class="title_lvl2">Simad University latest news and upcoming events</p>
        </div>
        <div class="col-sm-12 image_part head_bott">
          <div class="image_inner">
            <?php
						 // The Query
						 $home_post = 1;
						query_posts( array ( 'post_type' => 'post' , 'order' => '', 'posts_per_page' => 2, 'category__and' => array(1,2)) ); ?>
            <?php /* Start the Loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="<?=(($home_post%2==0) ? 'col-sm-12 image_right col-sm-12' : 'image_left'); ?>">
              <?php  
								//print_r($post);
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
									  if ($image) : ?>
              <div onclick="location.href='<?=$post->guid; ?>';" class="overflw"> <img class="img-responsive rght1" src="<?=get_site_url(); ?>/thumb/timthumb.php?src=<?php echo $image[0]; ?>&w=330&h=185&zc=1" alt="image"> </div>
              <?php  endif; ?>
              <p class="rght2">
                <?=date('F d, Y', strtotime($post->post_date));?>
              </p>
              <p class="rght3">
                <?=$post->post_title; ?>
              </p>
              <p class="rght4">
                <?=content(12);?>
              </p>
              <a class="news_readmor" href="<?=$post->guid; ?>">Read more</a> </div>
            <?php $home_post++; endwhile; ?>
            <?php wp_reset_query(); ?>
          </div>
        </div>
      </div>
      <div class="col-lg-4 mobiles1 head_bott">
        <div class="acadimic_inner">
          <div class="header_part pull-left"> <img class="img-responsive pull-left icon_img" src="<?php bloginfo( 'template_url' ); ?>/images/acdmic.png" alt="icon">
            <h3 class="title_lvl1">Academic Calendar</h3>
            <p class="title_lvl22">Programmes offered on Campus</p>
          </div>
          <div class="imag_right">
            <?php
						 // The Query
						 $home_postcal = 1;
						query_posts( array ( 'post_type' => 'post' , 'order' => '', 'posts_per_page' => 4, 'category__and' => array(3)) ); ?>
            <?php /* Start the Loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="decm">
              <div class="month_div_cal"><span>
                <?=date('M', strtotime($post->post_date));?>
                </span>
                <label>
                  <?=date('d', strtotime($post->post_date));?>
                </label>
              </div>
              <p><a href="<?=$post->guid; ?>">
                <?=$post->post_title; ?>
                </a></p>
            </div>
            <?php $home_postcal++; endwhile; ?>
            <?php wp_reset_query(); ?>
            <a href="<?=get_category_link(3); ?>" class="view_cpl hvr_clr">View Complete Calendar</a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--News &event end--> 

<!--Campus start-->

<div class="campus-facilities-focus-points">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4">
        <div class="our-facility-box campus-tour">
          <div class="img-wrpr"> <img src="<?php bloginfo( 'template_url' ); ?>/images/campus-tours.jpg" alt="campus tour"> </div>
          <div class="our-facilities-content-box">
            <div id="smallbox" onclick="location.href='#';" class="facilities-header"> <span class="facilities-icon campus-tour"></span>
              <header>
                <h3>Campus Tours</h3>
                <p>Take a campus tour</p>
                <a href="#">Schedule a Visit</a> </header>
            </div>
            <div class="facilities-content">
              <div class="re_button">
                <div class="re_button_but"> <a href="#">Take a campus tour</a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="our-facility-box academic-programmes">
        	<?php
					$my_category5 = get_term_by( 'id',5, 'academic-type' );					
                    $argsal5 = array(
					'posts_per_page' => 8, 'order' => '',
					'post_type' => 'academic-all',
					'tax_query' => array(
						array(
						'taxonomy' => 'academic-type',
						'field' => 'id',
						'terms' => 5
						 )
					  ),
					'suppress_filters' => true );				
					?>
          <div class="img-wrpr"> <?=((get_field('image', 'academic-type_5') ? '<img src="'.get_field('image', 'academic-type_5').'" alt="'.$my_category5->name.'">' : ''))?> </div>
          <div class="our-facilities-content-box">
            <div id="smallbox" onclick="location.href='#';" class="facilities-header"> <span class="facilities-icon academic-programmes"></span>
              <header>
                <h3><?=$my_category5->name; ?></h3>
                <p><?=$my_category5->description; ?></p>
                <a href="<?php bloginfo( 'url' ); ?>/academic-programs-simad-university/">Explore Programs</a> </header>
            </div>
            <div class="facilities-content">
              <ul>
                    <?php
					 // The Query
					 $ay5 = query_posts( $argsal5 );					
					 $posts_all5 = round(count($ay5)/2);
					 $ii5 = 1;
					 ?>
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?> 
                    	<li> <a href="<?php bloginfo( 'url' ); ?>/academic-programs-simad-university/#academic<?=$post->ID; ?>"><?php the_title();?></a> </li>
                    <?php echo (($posts_all5==$ii5) ? '</ul><ul>' : ''); 
					$ii5++; endwhile; ?>
				    <?php wp_reset_query(); ?>           
                
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="our-facility-box faculties">
        <?php
					$my_category = get_term_by( 'id',6, 'academic-type' );					
                    $argsal = array(
					'posts_per_page' => 8, 'order' => '',
					'post_type' => 'academic-all',
					'tax_query' => array(
						array(
						'taxonomy' => 'academic-type',
						'field' => 'id',
						'terms' => 6
						 )
					  ),
					'suppress_filters' => true );				
					?>
          <div class="img-wrpr"> <?=((get_field('image', 'academic-type_6') ? '<img src="'.get_field('image', 'academic-type_6').'" alt="'.$my_category->name.'">' : ''))?> </div>
          <div class="our-facilities-content-box">
            <div id="smallbox" onclick="location.href='#';" class="facilities-header"> <span class="facilities-icon facilities"></span>
              <header>
                <h3><?=$my_category->name; ?></h3>
                <p><?=$my_category->description; ?></p>
                <a href="<?php bloginfo( 'url' ); ?>/academic-programs-simad-university/">More About Faculties</a> </header>
            </div>
            <div class="facilities-content">
            
                    <ul>
                    <?php
					 // The Query
					 $ay = query_posts( $argsal );					
					 $posts_all = round(count($ay)/2);
					 $ii = 1;
					 ?>
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?> 
                    	<li> <a href="<?php bloginfo( 'url' ); ?>/academic-programs-simad-university/#academic<?=$post->ID; ?>"><?php the_title();?></a> </li>
                    <?php echo (($posts_all==$ii) ? '</ul><ul>' : ''); 
					$ii++; endwhile; ?>
				    <?php wp_reset_query(); ?>           
                
              </ul>
             
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Campus end-->
<div class="clearfix" />
<!--Testimonial start--> 
<!--<div class=""><img class="img-responsive" src="images/testimonial_banner.png" alt="image"></div>-->
<div class="testimonial_main">
  <div class="container head_bott">
    <div class="testi_title">
      <div class="testi_title_inner">
        <h3 class="testi_title1">Testimonials</h3>
        <p>What Students are saying!. Don't take our word for it.</p>
      </div>
    </div>
    <div id="our-testimonials" class="owl-carousel owl-theme our-testimonials">
      <?php
					 // The Query
					 $home_sl = 1;
					query_posts( array ( 'post_type' => 'testimonial-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
      <?php /* Start the Loop */ ?>
      <?php while ( have_posts() ) : the_post(); ?>
      <div class="item">
        <div class="indi-our-videos-wrapper">
          <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                          if ($image) : ?>
          <div class="img-wrpr">
            <?=((get_field('youtube_video_link',$post->ID)) ? '<a href="'.get_field('youtube_video_link',$post->ID).'" target="_blank" >' : '<a href="javascript:void(0);">' ); ?>
            <img src="<?=get_site_url(); ?>/thumb/timthumb.php?src=<?php echo $image[0]; ?>&w=490&h=280&zc=1" alt="<?php the_title();?>" /></a></div>
          <?php $home_sl++; endif; ?>
          <div class="content-wrpr">
            <header>
              <h3>
                <?php the_title();?>
              </h3>
              <?=((get_field('sub_heading',$post->ID)) ? '<p>'.get_field('sub_heading',$post->ID).' </p>' : '' ); ?>
            </header>
            <?php echo apply_filters('the_content', $post->post_content); ?> </div>
        </div>
      </div>
      <?php endwhile; ?>
      <?php wp_reset_query(); ?>
    </div>
  </div>
</div>
<!--Testimonial end--> 

<!--Facts start-->
<div class="fact_main">
  <div class="container fact_mob_res_cls head_bott">
    <div class="row">
      <div class="head_lin col-sm-12 wow fadeInUp head_bott">
        <p class="facts">Facts and Stats</p>
        <a>SIMAD University key facts and statistics</a> </div>
      <div class="col-lg-12 padding_lvl head_bott">
        <div class="col-lg-4 head_bott">
          <p class="inner1 wow fadeInUp"><a><img class="img-responsive prty" src="<?php bloginfo( 'template_url' ); ?>/images/pretty1.png" alt="image"></a></p>
          <p class="inner2 wow fadeInUp"><a><img class="img-responsive prty mob_ress" src="<?php bloginfo( 'template_url' ); ?>/images/pretty2.png" alt="image"></a></p>
          <p class="inner3 wow fadeInUp"><a><img class="img-responsive prty" src="<?php bloginfo( 'template_url' ); ?>/images/pretty3.png" alt="image"></a></p>
        </div>
        <div class="col-lg-4 head_bott">
          <p class="inner4 wow fadeInUp"><a><img class="img-responsive prty mob_ress" src="<?php bloginfo( 'template_url' ); ?>/images/pretty4.png" alt="image"></a></p>
          <p class="inner5 wow fadeInUp"><a><img class="img-responsive prty" src="<?php bloginfo( 'template_url' ); ?>/images/pretty5.png" alt="image"></a></p>
          <p class="inner6 wow fadeInUp"><a><img class="img-responsive prty" src="<?php bloginfo( 'template_url' ); ?>/images/pretty6.png" alt="image"></a></p>
        </div>
        <div class="col-lg-4 head_bott">
          <p class="inner7 wow fadeInUp"><a><img class="img-responsive prty" src="<?php bloginfo( 'template_url' ); ?>/images/pretty7.png" alt="image"></a></p>
          <p class="inner8 wow fadeInUp"><a><img class="img-responsive prty" src="<?php bloginfo( 'template_url' ); ?>/images/pretty8.png" alt="image"></a></p>
          <!--<p class="inner9 text_inn wow fadeInUp"><a href="">Explore More</a></p>--> 
        </div>
      </div>
    </div>
  </div>
</div>
<!--Facts end-->

<?php
get_footer();

