<?php
/**
 * Template Name: Graduates Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>

<div class="graduates_banner_main">
	<div class="container head_bott">
			<div class="graduates_banner_main_inn">
				<div class="col-lg-12 gradu_head head_bott"><?php echo apply_filters('the_content', $post->post_content); ?></div>
				<div class="col-lg-12 head_bott">
					<div class="col-lg-6 pull-left hide_leftt gra_mob">
						<div class="book_part">
                        	<?=((get_field('graduation_book_heading',$post->ID)) ? '<div class="book_part_head"><h2>'.get_field('graduation_book_heading',$post->ID).'</h2></div>' : '' ); ?>	
                            <?=((get_field('graduation_book_description',$post->ID)) ? '<div class="book_part_para"><p>'.get_field('graduation_book_description',$post->ID).'</p></div>' : '' ); ?>						
							<?=((get_field('graduation_book_image',$post->ID)) ? '<div class="book_part_img"><img src="'.get_field('graduation_book_image',$post->ID).'" class="img-responsive"/></div>' : '' ); ?>							
						</div>
						<div class="some_points">
							<div class="some_points_inn">
                            	<?=((get_field('some_of_the_book',$post->ID)) ? '<div class="some_points_head graduates_some_of_the_book"><a>'.get_field('some_of_the_book',$post->ID).'</a></div>' : '' ); ?>
								<?php if(get_field('some_of_the_book_list',$post->ID)){ 
											$list_f = explode(PHP_EOL,get_field('some_of_the_book_list',$post->ID));											
										?>
                                        	<div class="some_points_list">
									<ul>
                                            <?php foreach($list_f as $row){ ?>                                            
                                          		<li><a href="#"><?=$row; ?></a></li>
                                            <?php } ?>
                                            </ul></div>
                                        <?php } ?> 
								<?=((get_field('download_description',$post->ID)) ? '<div class="some_points_dwnp"><p class="some_points_dwnp_p1">'.get_field('download_description',$post->ID).'</p></div>' : '' ); ?>
								<?=((get_field('download_pdf',$post->ID)) ? '<div class="some_points_button"><a href="'.get_field('download_pdf',$post->ID).'">Download SU Graduation Book</a></div>' : '' ); ?>								
							</div>
						</div>
					</div>
					
					<div class="col-lg-6 pull-left hide_rightt mobil_ress">
						<div class="book_part">
                        	<?=((get_field('graduates_heading',$post->ID)) ? '<div class="book_part_head"><h2>'.get_field('graduates_heading',$post->ID).'</h2></div>' : '' ); ?>	
                            <?=((get_field('graduates_description',$post->ID)) ? '<div class="book_part_para add_another"><p>'.get_field('graduates_description',$post->ID).'</p></div>' : '' ); ?>						
							<?=((get_field('graduates_image',$post->ID)) ? '<div class="book_part_img"><img src="'.get_field('graduates_image',$post->ID).'" class="img-responsive"/></div>' : '' ); ?>							
						</div>
						<div class="faculty_sec">
							<div class="faculty_sec_inn">
                            	<?php if(get_field('faculty_description_1',$post->ID)){ ?>
								<div class="faculty_sec_main">
                                	<?=((get_field('faculty_heading_1',$post->ID)) ? '<a>'.get_field('faculty_heading_1',$post->ID).'</a>' : '' ); ?>	
                            		<?=((get_field('faculty_description_1',$post->ID)) ? '<p>'.get_field('faculty_description_1',$post->ID).'</p>' : '' ); ?>										
								</div>
                                <?php } ?>
                                
                                <?php if(get_field('faculty_description_2',$post->ID)){ ?>
								<div class="faculty_sec_main">
                                	<?=((get_field('faculty_heading_2',$post->ID)) ? '<a>'.get_field('faculty_heading_2',$post->ID).'</a>' : '' ); ?>	
                            		<?=((get_field('faculty_description_2',$post->ID)) ? '<p>'.get_field('faculty_description_2',$post->ID).'</p>' : '' ); ?>										
								</div>
                                <?php } ?>	
                                
                                <?php if(get_field('faculty_description_3',$post->ID)){ ?>
								<div class="faculty_sec_main">
                                	<?=((get_field('faculty_heading_3',$post->ID)) ? '<a>'.get_field('faculty_heading_3',$post->ID).'</a>' : '' ); ?>	
                            		<?=((get_field('faculty_description_3',$post->ID)) ? '<p>'.get_field('faculty_description_3',$post->ID).'</p>' : '' ); ?>										
								</div>
                                <?php } ?>	
                                
                                <?php if(get_field('faculty_description_4',$post->ID)){ ?>
								<div class="faculty_sec_main">
                                	<?=((get_field('faculty_heading_4',$post->ID)) ? '<a>'.get_field('faculty_heading_4',$post->ID).'</a>' : '' ); ?>	
                            		<?=((get_field('faculty_description_4',$post->ID)) ? '<p>'.get_field('faculty_description_4',$post->ID).'</p>' : '' ); ?>										
								</div>
                                <?php } ?>									
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
	</div>
</div>


<?php
get_footer();

