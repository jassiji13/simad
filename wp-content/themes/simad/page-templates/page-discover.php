<?php
/**
 * Template Name: Discover Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>
<div class="member_main bp-main-container">
	<div class="container">
		<div class="row">
        	<div class="member_main_inner academincs-main-container">
				<div class="col-lg-12 head_bott bp-accordion-nav-tabs-container">
					<div class="col-lg-3 bgm_whole1">
						<div class="member1">
							<nav id="nav">
								<ul class="dicover_submenus hover_stat_color nav">
                                	<?php
												 // The Query
												 $home_sl = 1;
												query_posts( array ( 'post_type' => 'discover-page-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  <li <?=(($home_sl==1) ? 'class="active"' : ''); ?>><a class="academic<?=$post->ID; ?>" data-toggle="tab" href="#academic<?=$post->ID; ?>"><?php the_title();?></a></li>								  
								  <?php $home_sl++; endwhile; ?>
								  <?php wp_reset_query(); ?>
								</ul>
							</nav>
						</div>
					</div>
					
					
					

					
					<div class="tab-content">
						
                        <?php
												 // The Query
												 $home_sltab = 1;
												query_posts( array ( 'post_type' => 'discover-page-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  
                                  <div id="academic<?=$post->ID; ?>" class="tab-pane fade in <?=(($home_sltab==1) ? 'active' : ''); ?>">
							<div class="col-lg-9 under_div">
								<div class="corporate-content-wrapper">
                                	<?php if($post->ID==158){ ?>	
                                      <div class="col-lg-6">
                                            <div class="member2">
                                                <div class="member_head">
                                                    <h2><?php the_title();?></h2>
                                                </div>
                                                <div class="member_para">
                                                    <?php echo apply_filters('the_content', $post->post_content); ?>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-lg-6 head_bott">
								<div class="mem_slider_inner">
									<div class="member3">
										<div class="member_last1"><?php if( function_exists('cyclone_slider') ) cyclone_slider('memberships'); ?> </div>
										<div class="member_last2">
											<?=((get_field('list_description_1',$post->ID)) ? '<p>'.get_field('list_description_1',$post->ID).'</p>' : '' ); ?>
										</div>
										<div class="member_last3 discover-membership-highlight-point">
											<?=((get_field('short_description',$post->ID)) ? '<h2>'.get_field('short_description',$post->ID).' </h2>' : '' ); ?>	
										</div>
									</div>
								</div>
							</div>
                            <div class="col-md-12">
                            	<?=((get_field('list_heading_1',$post->ID)) ? '<h3 style="font-size: 22px; margin-bottom: 20px;">'.get_field('list_heading_1',$post->ID).' </h3>' : '' ); ?>								
								<div class="owl-carousel owl-theme simad-member-universities" >
                                		<?php $q_args =  array ( 'post_type' => 'simad-member-all' , 'posts_per_page' => -1, 'order' => '' ) ;
										 $rpthumb_posts = get_posts($q_args);
										 ?>
                                        <?php foreach( $rpthumb_posts as $post_why){ //print_r($post_why); ?>                                    
										<?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_why->ID ), '' ); 
											    if ($image) : ?><div class="item"><img src="<?php echo $image[0]; ?>" data-src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div><?php  endif; ?>	
										<?php } ?>								    
								</div>
							</div>
                                       
                                  <?php }else{ ?>   
                                     
                                
									<div class="acdemic_head1">
										<h2><?php the_title();?></h2>
									</div>
                                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              			if ($image) : ?><div class="su-logo-wrpr"><img src="<?=$image[0]; ?>" /></div><?php endif; ?>	
                                        
                                        
									<div class="generic-content-writeup">
                                    	<?=((get_field('short_description',$post->ID)) ? '<p  class="add_para_sep1">'.get_field('short_description',$post->ID).' </p>' : '' ); ?>	
                                        								
										<?php echo apply_filters('the_content', $post->post_content); ?>
									</div>
                                    <?php if(get_field('list_heading_1',$post->ID) || get_field('list_description_1',$post->ID) || get_field('list_1',$post->ID)){ ?>
                                    <div class="corporate-content-box">									
										<?=((get_field('list_heading_1',$post->ID)) ? '<h2>'.get_field('list_heading_1',$post->ID).' </h2>' : '' ); ?>
                                        <?=((get_field('list_description_1',$post->ID)) ? '<p>'.get_field('list_description_1',$post->ID).'</p>' : '' ); ?>
                                        <?php if(get_field('list_1',$post->ID)){ ?>										
                                         <?php $list_1 = explode(PHP_EOL,get_field('list_1',$post->ID)); ?>
                                         <ul class="dotted-list"><?php foreach($list_1  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                          <?php } ?>
                                     </div>
                                     <?php } ?>
                                    
                                   
                                     
                                     
                                     <?php if(get_field('list_heading_2',$post->ID) || get_field('list_description_2',$post->ID) || get_field('list_2',$post->ID)){ ?>
                                    <div class="corporate-content-box">									
										<?=((get_field('list_heading_2',$post->ID)) ? '<h2>'.get_field('list_heading_2',$post->ID).' </h2>' : '' ); ?>
                                        <?=((get_field('list_description_2',$post->ID)) ? '<p>'.get_field('list_description_2',$post->ID).'</p>' : '' ); ?>
                                        <?php if(get_field('list_2',$post->ID)){ ?>										
                                         <?php $list_2 = explode(PHP_EOL,get_field('list_2',$post->ID)); ?>
                                         <ul class="dotted-list"><?php foreach($list_2  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                          <?php } ?>
                                     </div>
                                     <?php } ?>
                                     
                                     <?php if(get_field('list_heading_3',$post->ID) || get_field('list_description_3',$post->ID) || get_field('list_3',$post->ID)){ ?>
                                    <div class="corporate-content-box">									
										<?=((get_field('list_heading_3',$post->ID)) ? '<h2>'.get_field('list_heading_3',$post->ID).' </h2>' : '' ); ?>
                                        <?=((get_field('list_description_3',$post->ID)) ? '<p>'.get_field('list_description_3',$post->ID).'</p>' : '' ); ?>
                                        <?php if(get_field('list_3',$post->ID)){ ?>										
                                         <?php $list_3 = explode(PHP_EOL,get_field('list_3',$post->ID)); ?>
                                         <ul class="dotted-list"><?php foreach($list_3  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                          <?php } ?>
                                     </div>
                                     <?php } ?>
                                     
                                     
                                     <?php if(get_field('list_heading_4',$post->ID) || get_field('list_description_4',$post->ID) || get_field('list_4',$post->ID)){ ?>
                                    <div class="corporate-content-box">									
										<?=((get_field('list_heading_4',$post->ID)) ? '<h2>'.get_field('list_heading_4',$post->ID).' </h2>' : '' ); ?>
                                        <?=((get_field('list_description_4',$post->ID)) ? '<p>'.get_field('list_description_4',$post->ID).'</p>' : '' ); ?>
                                        <?php if(get_field('list_4',$post->ID)){ ?>										
                                         <?php $list_4 = explode(PHP_EOL,get_field('list_4',$post->ID)); ?>
                                         <ul class="dotted-list"><?php foreach($list_4  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                          <?php } ?>
                                     </div>
                                     <?php } ?>
                                     
                                     <?php if(get_field('list_heading_5',$post->ID) || get_field('list_description_5',$post->ID) || get_field('list_5',$post->ID)){ ?>
                                    <div class="corporate-content-box">									
										<?=((get_field('list_heading_5',$post->ID)) ? '<h2>'.get_field('list_heading_5',$post->ID).' </h2>' : '' ); ?>
                                        <?=((get_field('list_description_5',$post->ID)) ? '<p>'.get_field('list_description_5',$post->ID).'</p>' : '' ); ?>
                                        <?php if(get_field('list_5',$post->ID)){ ?>										
                                         <?php $list_5 = explode(PHP_EOL,get_field('list_5',$post->ID)); ?>
                                         <ul class="dotted-list"><?php foreach($list_5  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                          <?php } ?>
                                     </div>
                                     <?php } ?>
                                     <?php } ?>
                                     
								</div>
							</div>
						</div>
								  <?php $home_sltab++; endwhile; ?>
								  <?php wp_reset_query(); ?>
                        
                        
						

						
						
						
						
						
						
						
						
						
						
						
						

						

						
						
						
						
						
						
					</div>
					
					
				</div>
			</div>
        </div>
    </div>
</div>
<?php
get_footer();

