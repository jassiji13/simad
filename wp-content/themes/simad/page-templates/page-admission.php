<?php
/**
 * Template Name: Admission Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>
<div class="member_main bp-main-container">
	<div class="container">
		<div class="row">
        	<div class="member_main_inner academincs-main-container">
				<div class="col-lg-12 head_bott bp-accordion-nav-tabs-container">
					<div class="col-lg-3 bgm_whole1">
						<div class="member1">
							<nav id="nav">
								<ul class="dicover_submenus hover_stat_color nav">
                                	<?php
												 // The Query
												 $home_sl = 1;
												query_posts( array ( 'post_type' => 'admission-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  <li <?=(($home_sl==1) ? 'class="active"' : ''); ?>><a class="academic<?=$post->ID; ?>" data-toggle="tab" href="#academic<?=$post->ID; ?>"><?php the_title();?></a></li>								  
								  <?php $home_sl++; endwhile; ?>
								  <?php wp_reset_query(); ?>
								</ul>
							</nav>
						</div>
					</div>
					
					
					

					
					<div class="tab-content">
						
                        <?php
												 // The Query
												 $home_sltab = 1;
												query_posts( array ( 'post_type' => 'admission-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  
                                  <div id="academic<?=$post->ID; ?>" class="tab-pane fade in <?=(($home_sltab==1) ? 'active' : ''); ?>">
							<div class="col-lg-9 under_div">
								<div class="member2">
									<div class="acdemic_head1">
										<h2><?php the_title();?></h2>
									</div>
									<div class="acdemic_content1">
                                    	<?=((get_field('short_description',$post->ID)) ? '<p  class="add_para_sep1">'.get_field('short_description',$post->ID).' </p>' : '' ); ?>										
										<?php echo apply_filters('the_content', $post->post_content); ?>
									</div>
                                    <?php if($post->ID==123){ ?>
                                    <div class="why-choose-simad-highlight-features">
                                    	<?php $q_args =  array ( 'post_type' => 'why-choose-su-all' , 'posts_per_page' => -1 ) ;
										 $rpthumb_posts = get_posts($q_args);
										 ?>
                                        <?php foreach( $rpthumb_posts as $post_why){ //print_r($post_why); ?>                                    
										<div class="admiss_bgm1 pull-left admin_padd_bot1">
                                        	 <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_why->ID ), '' ); 
																  if ($image) : ?>
											  <div class="admiss_bgm_img pull-left"><img class="img-responsive" src="<?php echo $image[0]; ?>" data-src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>
											  <?php  endif; ?>	
											<div class="admiss_bgm_content pull-left">
												<div class="admiss_bgm_content_inn">
													<h2><?php $post_why->post_title;?></h2>
													<?php echo apply_filters('the_content', $post_why->post_content); ?>
												</div>
											</div>
										</div>
                                         
								  		<?php } ?>
									</div>
                                    <?php } ?>
                                    
                                    
                                    
                                    <?php if(get_field('requirement_list',$post->ID)){ ?>
                                        <div class="acdemic_content1">
                                            
                                            <?=((get_field('programs_heading',$post->ID)) ? '<p  class="add_para_sep3">'.get_field('programs_heading',$post->ID).' </p>' : '' ); ?>										
                                            <nav class="chang_cls_bgnd">
                                            	<?php $list_f = explode(PHP_EOL,get_field('requirement_list',$post->ID)); ?>
                                                <ul><?php foreach($list_f  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                            </nav>
                                        </div>
                                    <?php } ?>
								</div>
							</div>
						</div>
								  <?php $home_sltab++; endwhile; ?>
								  <?php wp_reset_query(); ?>
                        
                        
						

						
						
						
						
						
						
						
						
						
						
						
						

						

						
						
						
						
						
						
					</div>
					
					
				</div>
			</div>
        </div>
    </div>
</div>
<?php
get_footer();

