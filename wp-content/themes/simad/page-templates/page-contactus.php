<?php
/**
 * Template Name: Contact Us Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>



<div class="contact_banner_main">
	<div class="container head_bott">
			<div class="contact_banner_main_inn">
				<div class="col-lg-12 col-xs-12 contact_head head_bott">
					<?php echo  $post->post_content; ?>
				</div>
			</div>
			<div class="col-lg-12 col-xs-12 section_head head_bott con_mob contact-us-page-main-wrapper">
				<div class="col-lg-4 col-xs-12 head_bott">
                	<div class="contact_control contact-form-main-container"><?=do_shortcode('[contact-form-7 id="4" title="Contact form 1"]');?> </div></div>
				
				<div class="col-lg-4 col-xs-12">
					<div class="cent_contact">
						<div class="cent_contact1">
							<p class="cent_contact_p1">To contact <span>SIMAD UNIVERSITY,</span></p>
							<p class="cent_contact_p2">please use the contact details below:</p>
						</div>
						<div class="cent_contact2">
                        	<?=((get_field('telephone',$post->ID)) ? '<p class="cent_contact_p3"><span>Tel :</span>'.get_field('telephone',$post->ID).'</p>' : '' ); ?>  
                            <?=((get_field('fax',$post->ID)) ? '<p class="cent_contact_p4"><span>Fax :</span>'.get_field('fax',$post->ID).'</p>' : '' ); ?>							
						</div>
						<div class="cent_contact3">
                        	<?=((get_field('address',$post->ID)) ? '<p class="cent_contact_p5">P.O.Box: '.get_field('address',$post->ID).'</p>' : '' ); ?>
                            <?=((get_field('email_address',$post->ID)) ? '<a href="'.get_field('email_address',$post->ID).'" class="cent_contact_p6"><span>Email:</span> '.get_field('email_address',$post->ID).'</a>' : '' ); ?>							
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-xs-12 head_bott">
					<div id="map"></div>
				</div>
			</div>
	
	</div>
</div>



<script>
      function initMap() {
        var uluru = {lat: 2.066251, lng: 45.326493};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_RFcL_-MltUBji2c30dFOnB4YW2GYLGw&callback=initMap">
    </script>
<?php
get_footer();

