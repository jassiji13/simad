<?php
/**
 * Template Name: Corporate Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>
<div class="member_main bp-main-container">
	<div class="container">
		<div class="row">
        	<div class="member_main_inner academincs-main-container">
				<div class="col-lg-12 head_bott bp-accordion-nav-tabs-container">
					<div class="col-lg-3 bgm_whole1">
						<div class="member1">
							<nav id="nav">
								<ul class="dicover_submenus hover_stat_color nav">
                                	<?php
												 // The Query
												 $home_sl = 1;
												query_posts( array ( 'post_type' => 'corporate-page-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  <li <?=(($home_sl==1) ? 'class="active"' : ''); ?>><a class="academic<?=$post->ID; ?>" data-toggle="tab" href="#academic<?=$post->ID; ?>"><?php the_title();?></a></li>								  
								  <?php $home_sl++; endwhile; ?>
								  <?php wp_reset_query(); ?>
								</ul>
							</nav>
						</div>
					</div>
					
					
					

					
					<div class="tab-content">
						
                        <?php
												 // The Query
												 $home_sltab = 1;
												query_posts( array ( 'post_type' => 'corporate-page-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  
                                  <div id="academic<?=$post->ID; ?>" class="tab-pane fade in <?=(($home_sltab==1) ? 'active' : ''); ?>">
							<div class="col-lg-9 under_div">
								<div class="corporate-content-wrapper">
                                	<header class="bp-main-header">
								<h2><?php the_title();?></h2>
								<?=((get_field('short_description',$post->ID)) ? '<p  class="add_para_sep1">'.get_field('short_description',$post->ID).' </p>' : '' ); ?>
							</header>
								<?php if($post->ID==145){ ?>	
                                    <div class="cisco-academy-main-container">
								
								<?php echo apply_filters('the_content', $post->post_content); ?>


								<div class="col-md-6 corporate-writeup">									
                                    <?=((get_field('objectives_heading',$post->ID)) ? '<h3>'.get_field('objectives_heading',$post->ID).' </h3>' : '' ); ?>
                                    <?=((get_field('objectives_image',$post->ID)) ? '<img src="'.get_field('objectives_image',$post->ID).'" alt="">' : '' ); ?>
                                    <?php if(get_field('objectives_list',$post->ID)){ ?>										
                                     <?php $list_o = explode(PHP_EOL,get_field('objectives_list',$post->ID)); ?>
                                     <ul class="dotted-list"><?php foreach($list_o  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                      <?php } ?>
                                 </div>
								<div class="col-md-6 corporate-writeup">
									<?=((get_field('courses_heading',$post->ID)) ? '<h3>'.get_field('courses_heading',$post->ID).' </h3>' : '' ); ?>
                                    <?=((get_field('courses_image',$post->ID)) ? '<img src="'.get_field('courses_image',$post->ID).'" alt="">' : '' ); ?>
                                    <?php if(get_field('courses_list',$post->ID)){ ?>										
                                     <?php $list_c = explode(PHP_EOL,get_field('courses_list',$post->ID)); ?>
                                     <ul class="dotted-list"><?php foreach($list_c  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                      <?php } ?>
								</div>
								<?=((get_field('short_description_2',$post->ID)) ? '<p>'.get_field('short_description_2',$post->ID).' </p>' : '' ); ?>
								

							</div>
                                  <?php }else{ ?>  
									<div class="corporate-content-box">                                    										
										<?php echo apply_filters('the_content', $post->post_content); ?>
									</div>
                                    <?php if(get_field('vision_description',$post->ID)){ ?> 
                                   <div class="corporate-content-box">
									<?=((get_field('vision_heading',$post->ID)) ? '<h2>'.get_field('vision_heading',$post->ID).' </h2>' : '' ); ?>
									<?=((get_field('vision_description',$post->ID)) ? '<p>'.get_field('vision_description',$post->ID).' </p>' : '' ); ?>
								   </div>
                                     <?php } ?>
                                     
                                     <?php if(get_field('mission_description',$post->ID)){ ?> 
                                   <div class="corporate-content-box">
									<?=((get_field('mission_heading',$post->ID)) ? '<h2>'.get_field('mission_heading',$post->ID).' </h2>' : '' ); ?>
									<?=((get_field('mission_description',$post->ID)) ? '<p>'.get_field('mission_description',$post->ID).' </p>' : '' ); ?>
								   </div>
                                     <?php } ?>
                                    
                                        <div class="corporate-content-box">                                            
                                            <?=((get_field('objectives_heading',$post->ID)) ? '<h2>'.get_field('objectives_heading',$post->ID).' </h2>' : '' ); ?>
                                            <?php if(get_field('objectives_list',$post->ID)){ ?>										
                                            <nav class="chang_cls_bgnd">
                                            	<?php $list_o = explode(PHP_EOL,get_field('objectives_list',$post->ID)); ?>
                                                <ul><?php foreach($list_o  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                            </nav>
                                             <?php } ?>
                                        </div>
                                        <div class="corporate-content-box">                                            
                                            <?=((get_field('courses_heading',$post->ID)) ? '<h2>'.get_field('courses_heading',$post->ID).' </h2>' : '' ); ?>
                                            <?=((get_field('short_description_2',$post->ID)) ? '<p>'.get_field('short_description_2',$post->ID).' </p>' : '' ); ?>
                                            <?php if(get_field('courses_list',$post->ID)){ ?>										
                                            <nav class="chang_cls_bgnd">
                                            	<?php $list_f = explode(PHP_EOL,get_field('courses_list',$post->ID)); ?>
                                                <ul><?php foreach($list_f  as $list){ ?><li><?=$list; ?></li><?php } ?></ul>
                                            </nav>
                                             <?php } ?>
                                        </div>
                                   <?php } ?>
								</div>
							</div>
						</div>
								  <?php $home_sltab++; endwhile; ?>
								  <?php wp_reset_query(); ?>
                        
                        
						

						
						
						
						
						
						
						
						
						
						
						
						

						

						
						
						
						
						
						
					</div>
					
					
				</div>
			</div>
        </div>
    </div>
</div>
<?php
get_footer();

