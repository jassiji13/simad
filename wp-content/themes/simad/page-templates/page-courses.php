<?php
/**
 * Template Name: Courses Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<main class="bp-main-section">
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
        <header class="bp-banner-header bp-contact-us-header"  <?php if ($image){ ?>style="background:url(<?php echo $image[0]; ?>) center no-repeat; background-size: cover;" <?php } ?>>
            <div class="container">
                <div class="header-inner">
                    <!-- <i class="fa fa-users"></i> -->
                    <?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'">' : '' ); ?>
                    <h1><?php the_title();?></h1>
                </div>
            </div>
        </header>
        <section class="youth-courses-tabs-section">
            <div class="tabs-main-wrapper">
                <div class="container">
                    <ul>
                    	<?php
					 	// The Query
						query_posts( array ( 'post_type' => 'youth-course-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
						<?php /* Start the Loop */ ?>
						<?php $i =1; while ( have_posts() ) : the_post(); ?>     
                        	<li class="<?=(($i==1) ? 'active' : ''); ?> skills-<?=$post->ID; ?>"><a href="#skills-<?=$post->ID; ?>"><?php the_title();?></a></li>                
						<?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
							  if ($image) : ?>
							   <div><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>                                                     
							  <?php endif; ?> 
						 <?php $i++; endwhile; ?>
						<?php wp_reset_query(); ?>
                    </ul>
                </div>
            </div>

            <div class="tabs-content-wrapper youth-courses-content-wrapper">
            
            	<?php
					 	// The Query
						query_posts( array ( 'post_type' => 'youth-course-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
						<?php /* Start the Loop */ ?>
						<?php $i_ac =1; while ( have_posts() ) : the_post(); ?>
                        
                        <div class="tab-indi-wrapper <?=(($i_ac==1) ? 'active' : ''); ?>" id="skills-<?=$post->ID; ?>">
                    <div class="container" >
                        <header>
                            <h2><?php the_title();?></h2>
                            <?php the_content(); ?>
                        </header>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="youth-courses-info-box">
                                	<?=((get_field('features_heading',$post->ID)) ? '<header><h3>'.get_field('features_heading',$post->ID).'</h3></header>' : '' ); ?>                                    
                                    <div class="img-wrpr"><?=((get_field('features_image',$post->ID)) ? '<img src="'.get_field('features_image',$post->ID).'">' : '' ); ?></div>
                                    <div class="content-wrpr">
                                    	<?=((get_field('features_description',$post->ID)) ? '<p>'.get_field('features_description',$post->ID).'</p>' : '' ); ?> 
                                        <?php if(get_field('features_list',$post->ID)){ 
											$list_f = explode(PHP_EOL,get_field('features_list',$post->ID));											
										?>
                                        	<ul class="tick-listing">
                                            <?php foreach($list_f as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="youth-courses-info-box">
                                    <?=((get_field('benefits_heading',$post->ID)) ? '<header><h3>'.get_field('benefits_heading',$post->ID).'</h3></header>' : '' ); ?>                                    
                                    <div class="img-wrpr"><?=((get_field('benefits_image',$post->ID)) ? '<img src="'.get_field('benefits_image',$post->ID).'">' : '' ); ?></div>
                                    <div class="content-wrpr">
                                    	<?=((get_field('benefits_description',$post->ID)) ? '<p>'.get_field('benefits_description',$post->ID).'</p>' : '' ); ?> 
                                        <?php if(get_field('benefits_list',$post->ID)){ 
											$list_b = explode(PHP_EOL,get_field('benefits_list',$post->ID));											
										?>
                                        	<ul class="tick-listing">
                                            <?php foreach($list_b as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
						 <?php $i_ac++; endwhile; ?>
						<?php wp_reset_query(); ?>
            
           
            </div>
        </section>
        <?php if(get_field('english_skills_for_kids_ek_image')){ ?>
        <section class="youth-courses-english-courses-section">
            <div class="container">
            	<?=((get_field('english_skills_for_kids_ek_heading')) ? '<h2>'.get_field('english_skills_for_kids_ek_heading').'</h2>' : '' ); ?>
                <div class="english-skills-for-kids"><?=((get_field('english_skills_for_kids_ek_image')) ? '<img src="'.get_field('english_skills_for_kids_ek_image').'">' : '' ); ?></div>
            </div>
        </section>
        <?php } ?>

        <section class="adult-courses-social-activities youth-courses-social-activities">
        	<?php if(get_field('social_activities_image',$post->ID)){ ?>
			<style type="text/css">
				.adult-courses-social-activities.youth-courses-social-activities:after {
					background: url(<?=get_field('social_activities_image',$post->ID); ?>) no-repeat center center;
					background-size: cover;
				}
			</style>
            <?php } ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="social-activities-info-box" <?=((get_field('social_activities_icon')) ? 'style="background: url('.get_field('social_activities_icon').') no-repeat 0 0;"' : '' ); ?>>
                            <header>
                                <?=((get_field('social_activities_heading')) ? '<h2>'.get_field('social_activities_heading').'</h2>' : '' ); ?>
                                <?=((get_field('social_activities_description')) ? '<p>'.get_field('social_activities_description').'</p>' : '' ); ?>                               
                            </header>
                            <div class="social-activities-listing-box">
                                <?=((get_field('literacy_skills_heading')) ? '<h3>'.get_field('literacy_skills_heading').'</h3>' : '' ); ?>
                               <?php if(get_field('literacy_skills_list',$post->ID)){ 
											$list_ls = explode(PHP_EOL,get_field('literacy_skills_list',$post->ID));											
										?>
                                        	<ul>
                                            <?php foreach($list_ls as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>    
                            </div>
                            <div class="social-activities-listing-box">
                                <?=((get_field('communication_skills_heading')) ? '<h3>'.get_field('communication_skills_heading').'</h3>' : '' ); ?>
                                <?php if(get_field('communication_skills_list',$post->ID)){ 
											$list_cs = explode(PHP_EOL,get_field('communication_skills_list',$post->ID));											
										?>
                                        	<ul>
                                            <?php foreach($list_cs as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="adult-courses-delivery-option">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-8">
                        <header>
                            <?=((get_field('course_delivery_options_image',$post->ID)) ? '<img src="'.get_field('course_delivery_options_image',$post->ID).'">' : '' ); ?>
                            <div class="header-inner">
                                <?=((get_field('course_delivery_options_heading')) ? '<h3>'.get_field('course_delivery_options_heading').'</h3>' : '' ); ?>
                                <?=((get_field('course_delivery_options_description')) ? '<p>'.get_field('course_delivery_options_description').'</p>' : '' ); ?>
                            </div>
                        </header><?=((get_field('course_delivery_options_table')) ? get_field('course_delivery_options_table') : '' ); ?>                        
                    </div>
                    <div class="col-md-4">
                        <header>
                            <?=((get_field('course_fees_image_icon',$post->ID)) ? '<img src="'.get_field('course_fees_image_icon',$post->ID).'">' : '' ); ?>
                            <div class="header-inner">
                                <?=((get_field('course_fees_heading')) ? '<h3>'.get_field('course_fees_heading').'</h3>' : '' ); ?>
                            </div>
                        </header>
                        <?=((get_field('course_fees_list')) ? get_field('course_fees_list') : '' ); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="adult-courses-professional-development-courses">
            <div class="container">
                <div class="row">
                    <header>
                        <?=((get_field('professional_development_programs_heading')) ? '<h2>'.get_field('professional_development_programs_heading').'</h2>' : '' ); ?>
                        <?=((get_field('professional_development_programs_description')) ? '<p>'.get_field('professional_development_programs_description').'</p>' : '' ); ?>
                    </header>
                    <div class="col-md-6">
                        <div class="professional-development-info-box">
                           <?=((get_field('corporate_english_image',$post->ID)) ? '<img src="'.get_field('corporate_english_image',$post->ID).'">' : '' ); ?>
                            <div class="content-wrpr">
                                <?=((get_field('corporate_english_heading')) ? '<h3>'.get_field('corporate_english_heading').'</h3>' : '' ); ?>
                                <?=((get_field('corporate_english_description')) ? '<p>'.get_field('corporate_english_description').'</p>' : '' ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="professional-development-info-box">
                            <?=((get_field('teacher_training_image',$post->ID)) ? '<img src="'.get_field('teacher_training_image',$post->ID).'">' : '' ); ?>
                            <div class="content-wrpr">
                               <?=((get_field('teacher_training_heading')) ? '<h3>'.get_field('teacher_training_heading').'</h3>' : '' ); ?>
                                <?=((get_field('teacher_training_description')) ? '<p>'.get_field('teacher_training_description').'</p>' : '' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php if(get_field('contact_us_heading')){ ?>
			 <a href="<?php bloginfo( 'url' ); ?>/contact-us/"><div class="adult-courses-contact-us-for-more-info"><h3><?=get_field('contact_us_heading'); ?></h3></div></a> <?php } ?> 
    </main>
<?php
get_footer();

