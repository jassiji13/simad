<?php
/**
 * Template Name: Test English Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<main class="bp-main-section">
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
        <header class="bp-banner-header bp-contact-us-header"  <?php if ($image){ ?>style="background:url(<?php echo $image[0]; ?>) center no-repeat; background-size: cover;" <?php } ?>>
            <div class="container">
                <div class="header-inner">
                    <!-- <i class="fa fa-users"></i> -->
                    <?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'">' : '' ); ?>
                    <h1><?php the_title();?></h1>
                </div>
            </div>
        </header>
        <section class="test_english_sec">
                <div class="container">
                	<?php
					// Start the loop.
					while ( have_posts() ) : the_post();
			
						// Include the page content template.
						//get_template_part( 'content', 'page' );
						the_content();
						
						// If comments are open or we have at least one comment, load up the comment template.
						/*if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;*/
			
					// End the loop.
					endwhile;
					?>
                </div>
         </section>
</main>
<?php
get_footer();

