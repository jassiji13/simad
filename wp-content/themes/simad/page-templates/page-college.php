<?php
/**
 * Template Name: College Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<main class="bp-main-section">
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
        <header class="bp-banner-header bp-contact-us-header"  <?php if ($image){ ?>style="background:url(<?php echo $image[0]; ?>) center no-repeat; background-size: cover;" <?php } ?>>
            <div class="container">
                <div class="header-inner">
                    <!-- <i class="fa fa-users"></i> -->
                    <?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'">' : '' ); ?>
                    <h1><?php the_title();?></h1>
                </div>
            </div>
        </header>
        <section class="vision-mission-section">
            <div class="container">
            	<?=((get_field('short_description')) ? '<p class="highlight-para">'.get_field('short_description').'</p>' : '' ); ?>               
                <div class="row">
                    <div class="col-md-6">
                        <div class="vision-mission-box mission-box">
                            	<?=((get_field('our_mission_image')) ? '<img src="'.get_field('our_mission_image').'">' : '' ); ?>
                            <div class="content-wrpr">
                            	<?=((get_field('our_mission_heading')) ? '<h3>'.get_field('our_mission_heading').'</h3>' : '' ); ?>
                                <?=((get_field('our_mission_description')) ? '<p>'.get_field('our_mission_description').'</p>' : '' ); ?>                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="vision-mission-box vision-box">
                           		<?=((get_field('our_vision_image')) ? '<img src="'.get_field('our_vision_image').'">' : '' ); ?>
                            <div class="content-wrpr">
                            	<?=((get_field('our_vision_heading')) ? '<h3>'.get_field('our_vision_heading').'</h3>' : '' ); ?>
                                <?=((get_field('our_vision_description')) ? '<p>'.get_field('our_vision_description').'</p>' : '' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="highlight-para-box">
                    <?php
				// Start the Loop.
					while ( have_posts() ) : the_post();
	
						the_content();
						?>
                        <?php /*$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>
                              <script type="text/javascript">
								jQuery('.bp-topimage-header').parallax({imageSrc: '<?php echo $image[0]; ?>'});
								</script>                               
                                <?php endif; */?>
                        <?php
					endwhile;
				?>
                </div>
            </div>
        </section>
        <section class="our-collage-facilities" id="our-collage-facilities">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <header class="section-header">
                            <h2>Facilities and Services</h2>
                        </header>
                        <div class="facilities-services-main-wrapper">
                            <ul>
                            <?php
							 // The Query
							 $fs = array ( 'post_type' => 'facilities' , 'posts_per_page' => -1, 'order' => '' );
							$ay = query_posts($fs); ?>
							<?php /* Start the Loop */ 
							$posts_all = round(count($ay)/2);
							$ii = 1;
							 ?>
							<?php while ( have_posts() ) : the_post(); 
							 ?>   
                            <li>                  
							<?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
								  if ($image) : ?>
								   <span><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></span><span><?php the_title();?></span>                                                     
								  <?php endif; ?> 
                                  </li>
							 <?php echo (($posts_all==$ii) ? '</ul><ul>' : ''); $ii++; endwhile; ?>
							<?php wp_reset_query(); ?>
                            
                               
                            </ul>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <header class="section-header">
                            <h2>Did You Know?</h2>
                        </header>
                        <div class="did-you-know-carousel-wrapper">
                            <div id="did-you-know-carousel" class="slider-pro did-you-know-carousel">
                                    <div class="sp-slides">
                                    	<?php
										 // The Query
										query_posts( array ( 'post_type' => 'facility-slider' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
										<?php /* Start the Loop */ ?>
										<?php while ( have_posts() ) : the_post(); ?>                     
										<?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
											  if ($image) : ?>
                                              <div class="sp-slide">
                                              		<img class="sp-image" src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" />                                                   
                                                    <div class="sp-layer" data-position="leftBottom">
                                                        <p><?php the_title();?></p>    
                                                    </div>
                                                </div>                                                    
											  <?php endif; ?> 
										 <?php endwhile; ?>
										<?php wp_reset_query(); ?>
                                    
                                       
                                        <!-- END OF SLIDES -->
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php
get_footer();

