<?php
/**
 * Template Name: Adult Courses Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<main class="bp-main-section">
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
        <header class="bp-banner-header bp-contact-us-header"  <?php if ($image){ ?>style="background:url(<?php echo $image[0]; ?>) center no-repeat; background-size: cover;" <?php } ?>>
            <div class="container">
                <div class="header-inner">
                    <!-- <i class="fa fa-users"></i> -->
                    <?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'">' : '' ); ?>
                    <h1><?php the_title();?></h1>
                </div>
            </div>
        </header>
        <?php if($post->post_content){ ?><div class="adult-courses-intro">
            <div class="container">
               <?php echo apply_filters('the_content', $post->post_content); ?>
            </div>
        </div> <?php } ?>
        
        <div class="adult-courses-faq-tabs-section">
            <div class="container">
                <ul class="adult-courses-faq-tabs-listing">
                	<?php
					 	// The Query
						query_posts( array ( 'post_type' => 'adult-course-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
						<?php /* Start the Loop */ ?>
						<?php $i =1; while ( have_posts() ) : the_post(); ?>     
                        	<li class="<?=(($i==1) ? 'active' : ''); ?> skills-<?=$post->ID; ?>"><a href="#skills-<?=$post->ID; ?>"><h3><?php the_title();?></h3>
                            <?php if($post->post_content){ echo '<span>'.$post->post_content.'</span>'; } ?></a>
                            </li>        
						 
						 <?php $i++; endwhile; ?>
						<?php wp_reset_query(); ?>                       
                    
                </ul>
                <div class="tab-content-wrpr">
                	<?php
					 	// The Query
						query_posts( array ( 'post_type' => 'adult-course-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
						<?php /* Start the Loop */ ?>
						<?php $i_ac =1; while ( have_posts() ) : the_post(); ?>
                        
                        <div class="tab-indi-content-wrpr <?=(($i_ac==1) ? 'active' : ''); ?>" id="skills-<?=$post->ID; ?>">
                        <div class="intro-content-wrpr">
                            <?=((get_field('benefits_heading',$post->ID)) ? '<h3>'.get_field('benefits_heading',$post->ID).'</h3>' : '' ); ?>  
                            <?=((get_field('benefits_description',$post->ID)) ? '<p>'.get_field('benefits_description',$post->ID).'</p>' : '' ); ?> 
                            <div class="col-md-6">
                                <?=((get_field('benefits_image',$post->ID)) ? '<img src="'.get_field('benefits_image',$post->ID).'">' : '' ); ?>
                            </div>
                            
                            	<?php if(get_field('benefits_list',$post->ID)){ 
											$list_b = explode(PHP_EOL,get_field('benefits_list',$post->ID));											
										?>
                                        <div class="col-md-6">	<ul class="tick-listing">
                                            <?php foreach($list_b as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul></div>
                                        <?php } ?>                                 
                            
                        </div>
                        <div class="additional-info-content-wrpr">
                            <div class="col-md-6">
                                <div class="additional-info-inner">
                                    <?=((get_field('features_heading',$post->ID)) ? '<h3>'.get_field('features_heading',$post->ID).'</h3>' : '' ); ?>
                                    <?=((get_field('features_description',$post->ID)) ? '<p>'.get_field('features_description',$post->ID).'</p>' : '' ); ?> 
                                        <?php if(get_field('features_list',$post->ID)){ 
											$list_f = explode(PHP_EOL,get_field('features_list',$post->ID));											
										?>
                                        	<ul class="tick-listing">
                                            <?php foreach($list_f as $row){
												$list_i = explode(':',$row);
												 ?>                                            
                                          		<li><?=(($list_i[1]) ? '<h3>'.$list_i[0].':</h3><p>'.$list_i[1].'</p>' : '<p>'.$list_i[0].'</p>'); ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="additional-info-inner">
                                    <?=((get_field('business_communication_heading',$post->ID)) ? '<h3>'.get_field('business_communication_heading',$post->ID).'</h3>' : '' ); ?>
                                    <?php if(get_field('business_communication_list',$post->ID)){ 
											$list_bc = explode(PHP_EOL,get_field('business_communication_list',$post->ID));											
										?>
                                        	<ul class="tick-listing">
                                            <?php foreach($list_bc as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>  
                                </div>

                                <div class="additional-info-inner">
                                    <?=((get_field('career_skills_heading',$post->ID)) ? '<h3>'.get_field('career_skills_heading',$post->ID).'</h3>' : '' ); ?>
                                    <?php if(get_field('career_skills_list',$post->ID)){ 
											$list_ck = explode(PHP_EOL,get_field('career_skills_list',$post->ID));											
										?>
                                        	<ul class="tick-listing">
                                            <?php foreach($list_ck as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>  
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php $i_ac++; endwhile; ?>
						<?php wp_reset_query(); ?>
                    
                    
                    
                    
                </div>
            </div>
        </div>
        
        <?php if(get_field('english_skills_for_kids_ek_image')){ ?>
        <section class="youth-courses-english-courses-section">
            <div class="container">
            	<?=((get_field('english_skills_for_kids_ek_heading')) ? '<h2>'.get_field('english_skills_for_kids_ek_heading').'</h2>' : '' ); ?>
                <div class="english-skills-for-kids"><?=((get_field('english_skills_for_kids_ek_image')) ? '<img src="'.get_field('english_skills_for_kids_ek_image').'">' : '' ); ?></div>
            </div>
        </section>
		<?php } ?>
        <section class="adult-courses-social-activities youth-courses-social-activities">
        	<?php if(get_field('social_activities_image',$post->ID)){ ?>
			<style type="text/css">
				.adult-courses-social-activities.youth-courses-social-activities:after {
					background: url(<?=get_field('social_activities_image',$post->ID); ?>) no-repeat center center;
					background-size: cover;
				}
			</style>
            <?php } ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="social-activities-info-box" <?=((get_field('social_activities_icon')) ? 'style="background: url('.get_field('social_activities_icon').') no-repeat 0 0;"' : '' ); ?>>
                            <header>
                                <?=((get_field('social_activities_heading')) ? '<h2>'.get_field('social_activities_heading').'</h2>' : '' ); ?>
                                <?=((get_field('social_activities_description')) ? '<p>'.get_field('social_activities_description').'</p>' : '' ); ?>                               
                            </header>
                            <div class="social-activities-listing-box">
                                <?=((get_field('literacy_skills_heading')) ? '<h3>'.get_field('literacy_skills_heading').'</h3>' : '' ); ?>
                               <?php if(get_field('literacy_skills_list',$post->ID)){ 
											$list_ls = explode(PHP_EOL,get_field('literacy_skills_list',$post->ID));											
										?>
                                        	<ul>
                                            <?php foreach($list_ls as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>    
                            </div>
                            <div class="social-activities-listing-box">
                                <?=((get_field('communication_skills_heading')) ? '<h3>'.get_field('communication_skills_heading').'</h3>' : '' ); ?>
                                <?php if(get_field('communication_skills_list',$post->ID)){ 
											$list_cs = explode(PHP_EOL,get_field('communication_skills_list',$post->ID));											
										?>
                                        	<ul>
                                            <?php foreach($list_cs as $row){ ?>                                            
                                          		<li><?=$row; ?></li>
                                            <?php } ?>
                                            </ul>
                                        <?php } ?>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
         <section class="adult-courses-delivery-option">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-8">
                        <header>
                            <?=((get_field('course_delivery_options_image',$post->ID)) ? '<img src="'.get_field('course_delivery_options_image',$post->ID).'">' : '' ); ?>
                            <div class="header-inner">
                                <?=((get_field('course_delivery_options_heading')) ? '<h3>'.get_field('course_delivery_options_heading').'</h3>' : '' ); ?>
                                <?=((get_field('course_delivery_options_description')) ? '<p>'.get_field('course_delivery_options_description').'</p>' : '' ); ?>
                            </div>
                        </header><?=((get_field('course_delivery_options_table')) ? get_field('course_delivery_options_table') : '' ); ?>                        
                    </div>
                    <div class="col-md-4">
                        <header>
                            <?=((get_field('course_fees_image_icon',$post->ID)) ? '<img src="'.get_field('course_fees_image_icon',$post->ID).'">' : '' ); ?>
                            <div class="header-inner">
                                <?=((get_field('course_fees_heading')) ? '<h3>'.get_field('course_fees_heading').'</h3>' : '' ); ?>
                            </div>
                        </header>
                        <?=((get_field('course_fees_list')) ? get_field('course_fees_list') : '' ); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="adult-courses-professional-development-courses">
            <div class="container">
                <div class="row">
                    <header>
                        <?=((get_field('professional_development_programs_heading')) ? '<h2>'.get_field('professional_development_programs_heading').'</h2>' : '' ); ?>
                        <?=((get_field('professional_development_programs_description')) ? '<p>'.get_field('professional_development_programs_description').'</p>' : '' ); ?>
                    </header>
                    <div class="col-md-6">
                        <div class="professional-development-info-box">
                           <?=((get_field('corporate_english_image',$post->ID)) ? '<img src="'.get_field('corporate_english_image',$post->ID).'">' : '' ); ?>
                            <div class="content-wrpr">
                                <?=((get_field('corporate_english_heading')) ? '<h3>'.get_field('corporate_english_heading').'</h3>' : '' ); ?>
                                <?=((get_field('corporate_english_description')) ? '<p>'.get_field('corporate_english_description').'</p>' : '' ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="professional-development-info-box">
                            <?=((get_field('teacher_training_image',$post->ID)) ? '<img src="'.get_field('teacher_training_image',$post->ID).'">' : '' ); ?>
                            <div class="content-wrpr">
                               <?=((get_field('teacher_training_heading')) ? '<h3>'.get_field('teacher_training_heading').'</h3>' : '' ); ?>
                                <?=((get_field('teacher_training_description')) ? '<p>'.get_field('teacher_training_description').'</p>' : '' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php if(get_field('contact_us_heading')){ ?>
			 <a href="<?php bloginfo( 'url' ); ?>/contact-us/"><div class="adult-courses-contact-us-for-more-info"><h3><?=get_field('contact_us_heading'); ?></h3></div></a> <?php } ?>        
    </main>
<?php
get_footer();

