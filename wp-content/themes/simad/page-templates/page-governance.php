<?php
/**
 * Template Name: Governance Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>

<main class="governance-main-container">
    <div class="container">
    <div class="row senate-members-main-wrapper-container">
        <header>
        <h2>SU Senate</h2>
      </header>
        <div class="col-md-8 senate-members-sideways">
        <?php
												 // The Query
												 $home_sl = 1;
												query_posts( array ( 'post_type' => 'governance-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                          <a href="" class="senate-member" data-remodal-target="team-member-<?=$home_sl; ?>">
                          <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_why->ID ), '' ); 
											    if ($image) : ?><div class="img-wrpr"><img src="<?php echo $image[0]; ?>" data-src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div><?php  endif; ?>
          
          <div class="content-wrpr">
          <h3><?php the_title();?></h3>
          <p><?=content(25);?></p>
          <!-- <a href="">Know More</a> --> 
        </div>
          </a>             
                <?php if($home_sl==3 || $home_sl==8 || $home_sl==13 || $home_sl==18){ ?></div><div class="col-md-4 senate-members-vertical"><?php } ?>
                <?php if($home_sl==5 || $home_sl==10 || $home_sl==15 || $home_sl==20){ ?></div><div class="col-md-8 senate-members-sideways"><?php } ?>
                <?php $home_sl++; endwhile; ?>
				<?php wp_reset_query(); ?>
        </div>
       
        
      </div>
  </div>
  </main>
<?php
												 // The Query
												 $home_sl1 = 1;
												query_posts( array ( 'post_type' => 'governance-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
<div class="remodal team-member-wrpper" data-remodal-id="team-member-<?=$home_sl1; ?>">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="modal-inner-wrpr">
    <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_why->ID ), '' ); 
		   if ($image) : ?><div class="img-wrpr"><img src="<?php echo $image[0]; ?>" data-src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div><?php  endif; ?>
    <div class="content-wrpr">
        <h3><?php the_title();?></h3>
        <?php echo apply_filters('the_content', $post->post_content); ?>
      </div>
  </div>
  </div>
<?php $home_sl1++; endwhile; ?>
<?php wp_reset_query(); ?>
<?php
get_footer();

