<?php
/**
 * Template Name: About Us Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>


<div class="about_bann_main">
	<div class="container">
		<div class="row">
			<div class="out_sec about-us-content-main-outer-container">	
				<div class="col-lg-12 col-xs-12 head_bott ">
                	<?=((get_field('content_top')) ? '<div class="col-lg-12 col-xs-12 head_bott"><p class="banner_content1">'.get_field('content_top').'</p></div>' : '' ); ?>					
					
					<div class="col-lg-12 col-xs-12 head_bott">
						<div class="col-lg-6 col-xs-12  head_bott about-left-side-content">
                        	<?php the_content(); ?>							
						</div>
						<div class="col-lg-6 col-xs-12 head_bott about-right-side-content">
							<div class="vision-and-mission-box">
                            	<?=((get_field('vision_heading')) ? '<h2>'.get_field('vision_heading').'</h2>' : '' ); ?>	
                                <?=((get_field('vision_description')) ? '<p>'.get_field('vision_description').'</p>' : '' ); ?>								
							</div>
							<div class="vision-and-mission-box">
                            	<?=((get_field('mission_heading')) ? '<h2>'.get_field('mission_heading').'</h2>' : '' ); ?>	
                                <?=((get_field('mission_description')) ? get_field('mission_description') : '' ); ?>								
							</div>
						</div>
					</div>
				</div>
				<?=((get_field('content_bottom')) ? '<div class="col-lg-12 col-xs-12 head_bott"><p class="banner_content4">'.get_field('content_bottom').'</p></div>' : '' ); ?>
				
			</div>
		</div>
	</div>
</div>

<?php
get_footer();

