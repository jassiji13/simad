<?php
/**
 * Template Name: Team Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<main class="bp-main-section">
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
        <header class="bp-banner-header bp-contact-us-header"  <?php if ($image){ ?>style="background:url(<?php echo $image[0]; ?>) center no-repeat; background-size: cover;" <?php } ?>>
            <div class="container">
                <div class="header-inner">
                    <!-- <i class="fa fa-users"></i> -->
                    <?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'">' : '' ); ?>
                    <h1><?php the_title();?></h1>
                </div>
            </div>
        </header>
        <?php
		
        $rental_features = get_terms( 'team-category' );
 		foreach( $rental_features as $team){
			if($team->term_id==5){
		?>
         <section class="our-team-memebers" id="team<?=$team->term_id; ?>">
            <div class="container">
                <header class="section-header">
                    <h2><?=$team->name; ?></h2>
                </header>
                <div class="team-member-main-wrapper">
                	<?php
                    $argsal = array(
					'posts_per_page' => -1, 'order' => '',
					//'category' => 45,
					//'orderby' => 'post_date',
					//'order' => 'DESC',
					'post_type' => 'our-team-all',
					'tax_query' => array(
						array(
						'taxonomy' => 'team-category',
						'field' => 'id',
						'terms' => $team->term_id
						 )
					  ),
					/*'post_status' => 'draft, publish, future, pending, private',*/
					'suppress_filters' => true );			
					
					?>
                    
                    <?php
					 // The Query
					query_posts( $argsal ); ?>
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>  
                    <div class="team-member-indi">                   
                    <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                          if ($image) : ?>
                           <div class="img-wrpr"><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>                                                     
                          <?php endif; ?>   
                         <div class="content-wrpr">
                                <header>
                                    <h3><?php the_title();?></h3>
                                    <span>Executive Director</span>
                                </header>
                                <?php the_content(); ?>
                                <a href="#team<?=$post->ID; ?>">Know More</a>
                            </div>
                        </div> 
                        
                        <div class="remodal" data-remodal-id="team<?=$post->ID; ?>" role="dialog">
                        <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  						<div>
                        <?php if ($image) : ?>
                           <div class="img-wrpr"><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>                                                     
                          <?php endif; ?>
                        <h3><?php the_title();?></h3>
                         <?php the_content(); ?>
                         </div>
         				</div>                          
					 <?php endwhile; ?>
				    <?php wp_reset_query(); ?>
                </div>
            </div>
         </section>
         <?php }else{ ?>
         <section class="our-team-instructors" id="team<?=$team->term_id; ?>">
            <div class="container">
                <header class="section-header">
                    <h2><?=$team->name; ?></h2>
                </header>
                 <div class="our-team-instructors-wrapper">
                	<?php
                    $argsal = array(
					'posts_per_page' => -1, 'order' => '',
					//'category' => 45,
					//'orderby' => 'post_date',
					//'order' => 'DESC',
					'post_type' => 'our-team-all',
					'tax_query' => array(
						array(
						'taxonomy' => 'team-category',
						'field' => 'id',
						'terms' => $team->term_id
						 )
					  ),
					/*'post_status' => 'draft, publish, future, pending, private',*/
					'suppress_filters' => true );			
					
					?>
                    
                    <?php
					 // The Query
					query_posts( $argsal ); ?>
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>  
                    <div class="instructors-indi-box">               
                    <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                          if ($image) : ?>
                           <div class="img-wrpr"><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>                                                     
                          <?php endif; ?>   
                         <div class="content-wrpr">
                               <h3><?php the_title();?></h3>
                                 
                                <?php the_content(); ?>
                                <a href="#team<?=$post->ID; ?>">Know More</a>
                            </div>
                        </div> 
                        <div class="remodal" data-remodal-id="team<?=$post->ID; ?>" role="dialog">
                        <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  						<div>
                        <?php if ($image) : ?>
                           <div class="img-wrpr"><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>                                                     
                          <?php endif; ?>
                        <h3><?php the_title();?></h3>
                         <?php the_content(); ?>
                         </div>
         				</div>                         
					 <?php endwhile; ?>
				    <?php wp_reset_query(); ?>
                </div>
             </div>
         </section>
         <?php } ?>
         
        <?php } ?>        
    </main>
<?php
get_footer();

