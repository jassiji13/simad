<?php
/**
 * Template Name: Faqs Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
                              if ($image) : ?>

<?php endif; ?>

<div class="bp-banner-main-outer-container">
	<div class="container">
		<div class="bp-banner bp-about-banner" <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('header_icon')) ? '<img src="'.get_field('header_icon').'" class="img-responsive">' : '' ); ?>					
					<h1><?php the_title();?></h1>								 	
				 </div>
			</div>	
		</div>
	</div>
</div>
<main class="faq-main-container">	
	<div class="container">		
		<div class="row">
			<div class="col-md-12 faq-inner-main-container">
            <?php
												 // The Query
												 $home_sl = 1;
												query_posts( array ( 'post_type' => 'faq-all' , 'posts_per_page' => -1, 'order' => '' ) ); ?>
								  <?php /* Start the Loop */ ?>
								  <?php while ( have_posts() ) : the_post(); ?>
                                  				
				<div class="faq-indivisual-wrpr">					
					<a href="" class="faq-heading"><h3><?php the_title();?></h3></a>
					<div class="faq-content-wrpr"><?php echo apply_filters('the_content', $post->post_content); ?></div>
				</div>             
                
                <?php $home_sl++; endwhile; ?>
				<?php wp_reset_query(); ?>
            </div>
         </div>
         </div></main>



<?php
get_footer();

