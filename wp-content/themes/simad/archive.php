<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); 
$category = get_queried_object();	
?>

<div class="bp-banner-main-outer-container" >
	<div class="container">
		<div class="bp-banner bp-media-banner" <?=((get_field('banner_image',$category->taxonomy.'_'.$category->term_id)) ? 'style="background:url('.get_field('banner_image',$category->taxonomy.'_'.$category->term_id).') center bottom no-repeat; background-size: 100%;"' : '' ); ?>
         <?=(($image) ? 'style="background:url('.$image[0].') center bottom no-repeat; background-size: 100%;"' : ''); ?> >
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner">
                 	<?=((get_field('icon_image',$category->taxonomy.'_'.$category->term_id)) ? '<img src="'.get_field('icon_image',$category->taxonomy.'_'.$category->term_id).'" class="img-responsive"/>' : '' ); ?>					
					<h1><?php echo $category->name; ?></h1>							 	
				 </div>
			</div>	
		</div>
	</div>
</div>

<div class="member_main">
	<div class="container">
		<div class="row">
			<main class="col-md-12 bp-main-container">

				<header class="bp-main-header news-room-header">
                	<?=((get_field('sub_heading',$category->taxonomy.'_'.$category->term_id)) ? '<h2>'.get_field('sub_heading',$category->taxonomy.'_'.$category->term_id).'</h2>' : '' ); ?>					
                    <?php the_archive_description( '<p>', '</p>' ); ?>					
				</header>
				
					
					
                    
                    <?php if ( have_posts() ) : ?>
                    <?php
						// Start the Loop.
						echo '<div class="media-detials-main-container-wrapper">';
						while ( have_posts() ) : the_post();
			
							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							 
							get_template_part( 'content', get_post_format() );
			
						// End the loop.
						endwhile;
						echo '</div>';
						// Previous/next page navigation.
						the_posts_pagination( array(
							'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
							'next_text'          => __( 'Next page', 'twentyfifteen' ),
							'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
						) );
			
					// If no content, include the "No posts found" template.
					else :
						get_template_part( 'content', 'none' );
			
					endif;
					?>
				


			</main>	
		</div>
	</div>
</div>
<?php get_footer(); ?>
