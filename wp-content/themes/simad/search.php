<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="bp-banner-main-outer-container" >
	<div class="container">
		<div class="bp-banner bp-media-banner">
			<div class="bp-banner-info-box">
				 <div class="bp-infobox-inner"> 
                 	<img src="<?php bloginfo( 'template_url' ); ?>/images/res/media-banner-icon.png" />                					
					<h1><?php printf( __( 'Search Results for: %s', 'twentyfifteen' ), get_search_query() ); ?></h1>							 	
				 </div>
			</div>	
		</div>
	</div>
</div>

<div class="member_main">
	<div class="container">
		<div class="row">
			<main class="col-md-12 bp-main-container">
            <?php if ( have_posts() ) : ?>
            <?php
			// Start the loop.
			echo '<div class="media-detials-main-container-wrapper">';
			while ( have_posts() ) : the_post(); ?>

				<?php
				/*
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );

			// End the loop.
			endwhile;
echo '</div>';
			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
            </main>	
		</div>
	</div>
</div>

		

<?php get_footer(); ?>
