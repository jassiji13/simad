var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer');


gulp.task( 'prefix-css' , function() {
    gulp.src('style.css')
     .pipe(autoprefixer())
     .pipe(gulp.dest('prefixedcss'));
});