<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<div id="post-<?php the_ID(); ?>" class="col-md-4 col-sm-6">
						
						<a href="<?=$post->guid; ?>" class="media-indi-post">
							<header>
								<div class="date-wrpr">
									<span><?=date('M', strtotime($post->post_date));?></span>
									<span><?=date('d', strtotime($post->post_date));?></span>
								</div>
                                <?php the_title( '<h3>', '</h3>' ); ?>								
							</header>
                            <?php  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); 
							  if ($image) : ?>
							   <div class="img-wrpr"><img src="<?php echo $image[0]; ?>" alt="<?php the_title();?>" /></div>                                                     
							  <?php endif; ?> 
							<p><?=content(32); ?></p>
						</a>
</div>

