<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_simad');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']G?@LB{3PE-#+D|=zU!M6ScA-}5v-?KJa[9<`+lp| ggCnnC1)-t]}d_NV:BW#S=');
define('SECURE_AUTH_KEY',  'k7{I1}Nr1Sf+DU=@R!d4Peb)+y*45q. V7)v+0d6 coF3#V4dCy,jQ hp$&H2r$4');
define('LOGGED_IN_KEY',    '|g|zr7Bb5*gB)r8P9+-%O_C[kI&O:e6J~Iecc+@>?@PL(%w58.8Ao3-?o9UY}5Ow');
define('NONCE_KEY',        'j+3Aokm;sTpF?.8d%Xo9p6_`*D~rc&/?dJ!`q8lCU-@becO]kft<cL7z(*<  Het');
define('AUTH_SALT',        'ZC ZdW/Se>Jhu<O(xQpEv~(#gmEPYy?+g2X$o-wGfS+YHaqg{vcRxh0}zK|Lch?c');
define('SECURE_AUTH_SALT', 'LFpU4T4S)fIRCsV0B}%J;!42>X*=~.bh1YA^1EHc01$edZt.Mc<Q.yd4~$HDSg)O');
define('LOGGED_IN_SALT',   'I2~DRp~hRH]Ntxn0LIQ!MFjR/9kxT//s/=9jZJN-*u3>pF`im?`ui@_D]xacd*.G');
define('NONCE_SALT',       'EW5k;J5S+MzO= T-W<-pWcZ:*GSdVN9H/<[4`7^uv)&--46X]Osr~I/LcF1ghEe<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
